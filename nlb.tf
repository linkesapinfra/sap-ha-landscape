module "nlb_ascs" {
  source         = "git@bitbucket.org:linkesapinfra/nlb-for-sap-ha.git"
  overlai_ip_sap = "172.2.1.1"
  vpc_id         = module.vpc.vpc_id
  lb_name        = "ascs"
  project_code   = var.project_code
  subnets_lists  = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
  nlb_list       = {
    ascs-host-ctrl = {
      port_name   = "ascs-host-ctrl"
      port_number = 1128
    },
    ascs-gateway = {
      port_name   = "ascs-gateway"
      port_number = 3301
    },
    ascs-ms = {
      port_name   = "ascs-ms"
      port_number = 3601
    },
    ascs-ms-icm = {
      port_name   = "ascs-ms-icm"
      port_number = 8101
    },
    ascs-ms-icms = {
      port_name   = "ascs-ms-icms"
      port_number = 44301
    },
    ascs-srv-http = {
      port_name   = "ascs-srv-http"
      port_number = 50113
    },
    ascs-daa = {
      port_name   = "ascs-daa"
      port_number = 59813
    }
  }
}

module "nlb_ers" {
  source         = "git@bitbucket.org:linkesapinfra/nlb-for-sap-ha.git"
  overlai_ip_sap = "172.2.1.2"
  vpc_id         = module.vpc.vpc_id
  lb_name        = "ers"
  project_code   = var.project_code
  subnets_lists  = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
  nlb_list       = {
    ers-host-ctrl = {
      port_name   = "ers-host-ctrl"
      port_number = 1128
    },
    ers-gateway = {
      port_name   = "ers-gateway"
      port_number = 3302
    },
    ers-srv-http = {
      port_name   = "ers-srv-http"
      port_number = 50113
    },
    ers-daa = {
      port_name   = "ers-daa"
      port_number = 59813
    }
  }
}

module "nlb_hana" {
  source         = "git@bitbucket.org:linkesapinfra/nlb-for-sap-ha.git"
  overlai_ip_sap = "172.2.1.3"
  vpc_id         = module.vpc.vpc_id
  lb_name        = "hana"
  project_code   = var.project_code
  subnets_lists  = [module.vpc.private_subnets[0], module.vpc.private_subnets[1]]
  nlb_list       = {
    hdb-host-ctrl = {
      port_name   = "hdb-host-ctrl"
      port_number = 1128
    },
    hdb-http = {
      port_name   = "hdb-http"
      port_number = 8000
    },
    hdb-https = {
      port_name   = "hdb-https"
      port_number = 4300
    },
    hdb-namesrv = {
      port_name   = "hdb-namesrv"
      port_number = 30013
    },
    hdb-indexsrv = {
      port_name   = "hdb-indexsrv"
      port_number = 30015
    },
    hdb-srv-http = {
      port_name   = "hdb-srv-http"
      port_number = 50013
    },
    hdb-daa = {
      port_name   = "hdb-daa"
      port_number = 59813
    }
  }
}
