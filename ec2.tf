module "app_ascs_1" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile-ha.id
  name                   = "sap-ascs-01"
  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-ascs.abap-ascs-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "sapascs01"
      "Type"     = "appl"
      "SID"      = "LNS"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size          = "50"
  bin_volumes_count  = "1"
  bin_volumes_size   = "50"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_ascs_2" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile-ha.id
  name                   = "sap-ascs-02"
  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-ascs.abap-ascs-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "sapascs02"
      "Type"     = "appl"
      "SID"      = "LNS"
      "EFS"      = aws_efs_mount_target.efs_mount_point[1].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size          = "50"
  bin_volumes_count  = "1"
  bin_volumes_size   = "50"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_pas" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-appl-01"
  instance_type          = "r5.xlarge"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-appl.abap-appl-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "sapappl01"
      "Type"     = "appl"
      "SID"      = "LNS"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size          = "50"
  bin_volumes_count  = "1"
  bin_volumes_size   = "50"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_aas_1" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-appl-02"
  instance_type          = "r5.xlarge"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-appl.abap-appl-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "sapappl02"
      "Type"     = "appl"
      "SID"      = "LNS"
      "EFS"      = aws_efs_mount_target.efs_mount_point[1].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size          = "50"
  bin_volumes_count  = "1"
  bin_volumes_size   = "50"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}


module "app_hana_1" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-hana.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile-ha.id
  name                   = "sap-hana-01"
  instance_type          = "r5.4xlarge"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-hana.hana-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "saphana01"
      "Type"     = "hana"
      "SID"      = "LNH"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size            = "50"
  bin_volumes_count    = "1"
  bin_volumes_size     = "50"
  data_volumes_count   = "3"
  data_volumes_size    = "225"
  logs_volumes_count   = "2"
  logs_volumes_size    = "150"
  shared_volumes_count = "1"
  shared_volumes_size  = "300"
  backup_volumes_count = "1"
  backup_volumes_size  = "512"
}

module "app_hana_2" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-hana.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile-ha.id
  name                   = "sap-hana-02"
  instance_type          = "r5.4xlarge"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.private_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-hana.hana-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "saphana02"
      "Type"     = "hana"
      "SID"      = "LNH"
      "EFS"      = aws_efs_mount_target.efs_mount_point[1].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  # Define EBS volumes
  root_size            = "50"
  bin_volumes_count    = "1"
  bin_volumes_size     = "50"
  data_volumes_count   = "3"
  data_volumes_size    = "225"
  logs_volumes_count   = "2"
  logs_volumes_size    = "150"
  shared_volumes_count = "1"
  shared_volumes_size  = "300"
  backup_volumes_count = "1"
  backup_volumes_size  = "512"
}

module "app_web_dispatcher" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-wd-01"
  instance_type          = "t3.small"
#  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.public_subnets[0]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-wd.wd-id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "testwdinst"
      "Type"     = "appl"
      "SID"      = "LNW"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  public_ip              = true
  # Define EBS volumes
  root_size          = "10"
  bin_volumes_count  = "1"
  bin_volumes_size   = "25"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}

module "app_saprouter" {
  source                 = "git@bitbucket.org:linkesapinfra/ec2-sap-appl.git"
  project_code           = var.project_code
  instance_profile_name  = module.iam-profile.id
  name                   = "sap-sr-01"
  instance_type          = "t3.small"
#  instance_type          = "r5.large"
  ami_id                 = var.ami-id
  subnet_id              = module.vpc.public_subnets[1]
  vpc_security_group_ids = [module.sg-admin.admin-id, module.sg-saprouter.id]
  aws_kms_id             = data.aws_kms_key.ebs.arn
  key_pair               = aws_key_pair.auth.id
  autorecover            = true
  common_tags = merge(
    var.global_tags, tomap({
      "Hostname" = "saprouter"
      "Type"     = "appl"
      "SID"      = "LNW"
      "EFS"      = aws_efs_mount_target.efs_mount_point[0].dns_name
      })
  )
  user_data_file         = "user_data.sh"
  public_ip              = true
  # Define EBS volumes
  root_size          = "10"
  bin_volumes_count  = "1"
  bin_volumes_size   = "10"
  swap_volumes_count = "1"
  swap_volumes_size  = "20"
}
