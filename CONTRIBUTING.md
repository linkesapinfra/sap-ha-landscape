# Contribution Guidelines
👍🎉 First off, thanks for taking the time to contribute! 🎉👍

Please ensure your pull request adheres to the following guidelines:

- _Send an email to authors to discuss_ what you would like to change
- Alphabetize your entry.
- Search previous suggestions and current roadmap before making a new one, as yours may be a duplicate.
- Make an individual pull request for each suggestion.
- Keep descriptions short and simple, but descriptive.
- Start the description with a capital and end with a full stop/period.
- Check your spelling and grammar.
- Make sure to update tests as appropriate

Thank you for your suggestions!
