module "vpc" {
  source = "terraform-aws-modules/vpc/aws"  // https://registry.terraform.io/modules/terraform-aws-modules/vpc/aws/latest

  name = format("%s-%s", module.prefix.vpc, var.vpc_name)
  cidr = "172.1.0.0/16"

  azs             = ["eu-west-1a", "eu-west-1b"]
  private_subnets = ["172.1.1.0/24", "172.1.2.0/24"]
  public_subnets  = ["172.1.11.0/24", "172.1.12.0/24"]

  enable_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

// Please do not use the tag "Name" becouse is inclued in the module code
  tags = var.global_tags
}
