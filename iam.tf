module "iam-profile" {
  source            = "git@bitbucket.org:linkesapinfra/iam-profile-sap.git"
  name              = "sap-prf-default"
  is_ha             = false
  kms_list          = [data.aws_kms_key.ebs.arn, data.aws_kms_key.efs.arn, data.aws_kms_key.s3.arn]
  global_tags       = var.global_tags
}

module "iam-profile-ha" {
  source            = "git@bitbucket.org:linkesapinfra/iam-profile-sap.git"
  name              = "sap-prf-default"
  is_ha             = true
  kms_list          = [data.aws_kms_key.ebs.arn, data.aws_kms_key.efs.arn, data.aws_kms_key.s3.arn]
  routetables_list  = module.vpc.private_route_table_ids
  instance_list     = [module.app_ascs_1.id, module.app_ascs_2.id, module.app_hana_1.id, module.app_hana_2.id]
  global_tags       = var.global_tags
}
